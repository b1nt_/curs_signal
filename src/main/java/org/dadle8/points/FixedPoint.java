package org.dadle8.points;

public class FixedPoint {

    private int maskInteger;
    private int maskFractional;
    private int singMask;
    private int valueInBits;
    private String value;
    private int iN;
    private int fM;

    public FixedPoint(int iN, int fM, String value) {
        this.value = value;
        this.iN = iN;
        this.fM = fM;
        maskInteger = getMask(iN);
        maskFractional = getMask(fM);
        singMask = (int) Math.pow(2, iN + fM);

        int s = getBinarySingPart();
        int i = getBinaryIntegerPart();
        int f = getBinaryFractionalPart();
        valueInBits = s | i | f;
    }

    private int getBinarySingPart() {
        return ((value.startsWith("-") ? 1 : 0 ) << (iN + fM)) & singMask;
    }

    private int getBinaryIntegerPart() {
        String temp = value.substring(0, value.indexOf("."));
        if (temp.startsWith("-")) {
            temp = Integer.toString(Integer.parseInt(temp.substring(1)));
        }

        int tempInteger = Integer.parseInt(temp);
        return (tempInteger & maskInteger) << fM;
    }

    private int getBinaryFractionalPart() {
        Double fractional = Double.parseDouble("0." + value.substring(value.indexOf(".") + 1));
        Double temp = fractional * 2;
        int[] fractionalInBinary = new int[fM];
        for(int i = 0; i < fM; i++) {
            if (temp.toString().startsWith("1")) {
                fractionalInBinary[i] = 1;
                temp = Double.parseDouble("0." + temp.toString().substring(temp.toString().indexOf(".") + 1));
            } else {
                fractionalInBinary[i] = 0;
            }
            if (temp.toString().substring(temp.toString().indexOf(".") + 1).length() == 1
                    && temp.toString().substring(temp.toString().indexOf(".") + 1).equals("0")) {
                break;
            }
            temp = temp * 2;
        }

        int integer = 0;
        for(int i = 0; i < fractionalInBinary.length; i++) {
            integer += fractionalInBinary[i] * Math.pow(2, fractionalInBinary.length - i - 1);
        }
        return integer;
    }

    private String singPartToBinaryString() {
        return valueInBits >> (fM + iN) == 0 ? "" : "-";
    }

    private String integerPartToBinarySting() {
        char[] string = new char[iN];
        int temp = valueInBits >> (fM);
        for(int i = 0; i < string.length; i++) {
            string[string.length - i - 1] = (temp & 0x00000001) == 0 ? '0' : '1';
            temp = temp >> 1;
        }
        return String.valueOf(string);
    }

    private String fractionalPartToBinaryString() {
        char[] string = new char[fM];
        int temp = valueInBits;
        int mask = (int) Math.pow(2, fM - 1);
        for(int i = 0; i < string.length; i++) {
            string[i] = ((temp & mask) >> fM - 1) == 0 ? '0' : '1';
            temp = temp << 1;
        }
        return String.valueOf(string);
    }

    public String getBinaryStringValue() {
        return String.format("%s %s.%s", singPartToBinaryString(), integerPartToBinarySting(), fractionalPartToBinaryString());
    }

    public String getValue() {
        int integer = 0;
        char[] intPart = integerPartToBinarySting().toCharArray();
        for(int i = 0; i < iN; i++) {
            integer += intPart[i] == '0' ? 0 : 1 * Math.pow(2, iN - i - 1);
        }

        Double fractional = 0.0;
        char[] fracPart = fractionalPartToBinaryString().toCharArray();
        for(int k = 0; k < fM; k++) {
            fractional += (double) fracPart[k] == '0' ? 0 : 1 * Math.pow(2, -1 - k);
        }
        return singPartToBinaryString() + integer + fractional.toString().substring(fractional.toString().indexOf("."));
    }

    public String getFixedPointValueWithTruncated(int trunk) {
        int fractionalPart = valueInBits & maskFractional;
        fractionalPart = (fractionalPart >> trunk) << trunk;
        int temp = valueInBits;
        valueInBits = getBinarySingPart() | getBinaryIntegerPart() | fractionalPart;
        String str = getValue();
        valueInBits = temp;
        return str;
    }

    public String getFixedPointValueWithTruncatedAndPlusOne(int trunk) {
        int fractionalPart = valueInBits & maskFractional;
        fractionalPart = ((fractionalPart >> trunk) + 1) << trunk;
        int temp = valueInBits;
        valueInBits = getBinarySingPart() | getBinaryIntegerPart() | fractionalPart;
        String str = getValue();
        valueInBits = temp;
        return str;
    }

    private int getMask(int shift) {
        int temp = 0;
        for(int i = 0; i < shift; i++) {
            temp <<= 1;
            temp |= 1;
        }
        return temp;
    }
}
