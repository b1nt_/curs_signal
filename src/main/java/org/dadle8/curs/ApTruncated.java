package org.dadle8.curs;

public interface ApTruncated {
    double[] executeWithFixedPointAndTruncated(double[] a, double b[], int trunk);
    double[] executeWithFixedPointAndTruncatedAndPlusOne(double[] a, double b[], int trunk);
    double[] executeWithFixedPointWithoutResult(double[] a, double b[], int in1, int fn1, int in2, int fn2);
    double[] executeWithRound(double[] a, double b[], int in1, int fn1, int in2, int fn2);
}
