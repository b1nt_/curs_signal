package org.dadle8.curs;

public interface ApConvilution {
    double[] execute(double[] a, double b[]);
    double[] executeWithFixedPoint(double[] a, double b[]);
}
