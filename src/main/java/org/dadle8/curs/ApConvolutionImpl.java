package org.dadle8.curs;

import org.dadle8.points.FixedPoint;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ApConvolutionImpl implements ApConvilution, ApTruncated {

    private static final Logger logger = Logger.getLogger(ApConvolutionImpl.class.getName());

    public ApConvolutionImpl(Level logLevel) {
        logger.setLevel(logLevel);
        logger.addHandler(new java.util.logging.ConsoleHandler());
        logger.setUseParentHandlers(false);
    }

    public double[] execute(double[] a, double b[]) {
        double[] s = new double[a.length + b.length - 1];

        for (int n = 0; n < s.length; n++) {
            for(int m = 0; m <= n; m++) {
                double a_actual = m > a.length - 1 ? 0 : a[m];
                double b_actual = n - m > b.length - 1 ? 0 : b[n - m];
                s[n] += a_actual * b_actual;
            }
            logger.info("Value s[" + n + "]: " + Double.toString(s[n]));
        }
        return s;
    }

    public double[] executeWithFixedPoint(double[] a, double b[]) {
        double[] s = executeWithFixedPointWithoutResult(a, b);

        for(int i = 0; i < s.length; i++) {
            s[i] = Double.parseDouble(new FixedPoint(3, 8, Double.toString(s[i])).getValue());
        }
        return s;
    }

    public double[] executeWithFixedPointAndTruncated(double[] a, double b[], int trunk) {
        double[] s = executeWithFixedPointWithoutResult(a, b);

        for(int i = 0; i < s.length; i++) {
            s[i] = Double.parseDouble(new FixedPoint(3,8, Double.toString(s[i])).getFixedPointValueWithTruncated(trunk));
        }
        return s;
    }

    public double[] executeWithFixedPointAndTruncatedAndPlusOne(double[] a, double b[], int trunk) {
        double[] s = executeWithFixedPointWithoutResult(a, b);

        for(int i = 0; i < s.length; i++) {
            s[i] = Double.parseDouble(new FixedPoint(3,8, Double.toString(s[i])).getFixedPointValueWithTruncatedAndPlusOne(trunk));
        }
        return s;
    }

    private double[] executeWithFixedPointWithoutResult(double[] a, double b[]) {
        return executeWithFixedPointWithoutResult(a, b, 2, 5, 0, 3);
    }

    public double[] executeWithFixedPointWithoutResult(double[] a, double b[], int in1, int fn1, int in2, int fn2) {
        double[] a_actual = new double[a.length];
        for(int i = 0; i < a.length; i++) {
            a_actual[i] = Double.parseDouble((new FixedPoint(in1, fn1, Double.toString(a[i]))).getValue());
        }
        double[] b_actual = new double[b.length];
        for(int i = 0; i < b.length; i++) {
            b_actual[i] = Double.parseDouble((new FixedPoint(in2, fn2, Double.toString(b[i]))).getValue());
        }

        double[] s = new double[a.length + b.length - 1];
        for (int n = 0; n < s.length; n++) {
            for(int m = 0; m <= n; m++) {
                double a_ac = m > a.length - 1 ? 0 : a_actual[m];
                double b_ac = n - m > b.length - 1 ? 0 : b_actual[n - m];
                s[n] += Double.parseDouble((new FixedPoint(3, 8, Double.toString(a_ac * b_ac))).getValue());
            }
            logger.info("Value s[" + n + "]: " + Double.toString(s[n]));
        }
        return s;
    }

    public double[] executeWithRound(double[] a, double b[]) {
        return executeWithRound(a, b, 2, 5, 0, 3);
    }

    public double[] executeWithRound(double[] a, double b[], int in1, int fn1, int in2, int fn2) {
        double[] s = executeWithFixedPointWithoutResult(a, b, in1, fn1, in2, fn2);

        DecimalFormat decimalFormat = new DecimalFormat("#0.0");
        decimalFormat.setDecimalFormatSymbols(DecimalFormatSymbols.getInstance(Locale.ENGLISH));
        for(int i = 0; i < s.length; i++) {
            s[i] = Double.parseDouble(decimalFormat.format(Double.parseDouble(new FixedPoint(3, 8, Double.toString(s[i])).getValue())));
        }
        return s;
    }
}
