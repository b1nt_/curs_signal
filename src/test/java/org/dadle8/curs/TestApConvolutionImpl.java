package org.dadle8.curs;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.logging.Level;

public class TestApConvolutionImpl {

    private final static String CSVFILENAME = "report";
    private final static String POSTFIX = ".csv";

    private ApConvolutionImpl apConvolution;
    private double[] b = { -0.2, -0.2, 0.8, -0.2, -0.2 };
    private double[][] aArrays = {
            { 2.0, 1.0, 3.0, -1.0, 4.0, 2.5, 1.5, 3.5, -1.5, 4.5 },
            { 2.0, 1.0, 3.0, -1.0, 4.0, 2.5, 1.5, 3.5, -1.5, 4.5, 3.3, -5.7, 4.2, -2.8, 7.7 },
            { 2.0, 1.0, 3.0, -1.0, 4.0, 2.5, 1.5, 3.5, -1.5, 4.5, 3.3, -5.7, 4.2, -2.8, 7.7, -4.5, 3.7, -0.5, 0.7, 2.7 },
            { 2.0, 1.0, 3.0, -1.0, 4.0, 2.5, 1.5, 3.5, -1.5, 4.5, 3.3, -5.7, 4.2, -2.8, 7.7, -4.5, 3.7, -0.5, 0.7, 2.7, 2.0, 1.0, 3.0, -1.0, 4.0 },
            { 2.0, 1.0, 3.0, -1.0, 4.0, 2.5, 1.5, 3.5, -1.5, 4.5, 3.3, -5.7, 4.2, -2.8, 7.7, -4.5, 3.7, -0.5, 0.7, 2.7, 2.0, 1.0, 3.0, -1.0, 4.0, 2.5, 1.5, 3.5, -1.5, 4.5 },
            { 2.0, 1.0, 3.0, -1.0, 4.0, 2.5, 1.5, 3.5, -1.5, 4.5, 3.3, -5.7, 4.2, -2.8, 7.7, -4.5, 3.7, -0.5, 0.7, 2.7, 2.0, 1.0, 3.0, -1.0, 4.0, 2.5, 1.5, 3.5, -1.5, 4.5, 3.3, -5.7, 4.2, -2.8, 7.7 },
            { 2.0, 1.0, 3.0, -1.0, 4.0, 2.5, 1.5, 3.5, -1.5, 4.5, 3.3, -5.7, 4.2, -2.8, 7.7, -4.5, 3.7, -0.5, 0.7, 2.7, 2.0, 1.0, 3.0, -1.0, 4.0, 2.5, 1.5, 3.5, -1.5, 4.5, 3.3, -5.7, 4.2, -2.8, 7.7, -4.5, 3.7, -0.5, 0.7, 2.7 }
    };
    private int[][] bitness = {
            { 1, 4, 0, 3 },
            { 3, 6, 0, 3 },
            { 2, 5, 0, 1 },
            { 2, 5, 1, 4 }
    };

    @BeforeClass
    public static void initTest() {
        clearCSVFile();
    }

    @Before
    public void beforeTest() {
        apConvolution = new ApConvolutionImpl(Level.OFF);
    }

    @After
    public void afterTest() {
        apConvolution = null;
    }

    @Test
    public void test_do_it() {
        test_ch_aperiodic_convolution();
        test_ch_aperiodic_convolution_with_trunk();
        test_ch_aperiodic_convolution_with_trunk_and_plus_one();
        test_ch_aperiodic_convolution_with_round();
        test_ch_aperiodic_convolution_with_round_for_2bit_and_4bit();
    }

    @Test
    public void test_ch_aperiodic_convolution() {
        for (double[] a : aArrays) {
            toCSVFile(Arrays.toString(apConvolution.execute(a, b)));
            toCSVFile(Arrays.toString(apConvolution.executeWithFixedPoint(a, b)));
        }
    }

    @Test
    public void test_ch_aperiodic_convolution_with_trunk() {
        for (double[] a : aArrays) {
            toCSVFile(Arrays.toString(apConvolution.execute(a, b)), CSVFILENAME + "1");
            toCSVFile(Arrays.toString(apConvolution.executeWithFixedPointAndTruncated(a, b, 6)), CSVFILENAME + "1");
        }
    }

    @Test
    public void test_ch_aperiodic_convolution_with_trunk_and_plus_one() {
        for (double[] a : aArrays) {
            toCSVFile(Arrays.toString(apConvolution.execute(a, b)), CSVFILENAME + "2");
            toCSVFile(Arrays.toString(apConvolution.executeWithFixedPointAndTruncatedAndPlusOne(a, b, 6)), CSVFILENAME + "2");
        }
    }

    @Test
    public void test_ch_aperiodic_convolution_with_round() {
        for (double[] a : aArrays) {
            toCSVFile(Arrays.toString(apConvolution.execute(a, b)), CSVFILENAME + "3");
            toCSVFile(Arrays.toString(apConvolution.executeWithRound(a, b)), CSVFILENAME + "3");
        }
    }

    @Test
    public void test_ch_aperiodic_convolution_with_round_for_2bit_and_4bit() {
        int i = 4;
        for(int[] bits : bitness) {
            for (double[] a : aArrays) {
                toCSVFile(Arrays.toString(apConvolution.execute(a, b)), CSVFILENAME + i);
                toCSVFile(Arrays.toString(apConvolution.executeWithRound(a, b, bits[0], bits[1], bits[2], bits[3])), CSVFILENAME + i);
            }
            i++;
        }
    }

    private void toCSVFile(String str, String filename) {
        File cvs = new File(filename + POSTFIX);
        try {
            if (!cvs.exists()) {
                cvs.createNewFile();
            }
            str = str.replace("[", "").replace("]", "").replaceAll(", ", ";").replace(".", ",");

            FileWriter writer = new FileWriter(cvs, true);
            BufferedWriter bufferWriter = new BufferedWriter(writer);
            bufferWriter.write(str);
            bufferWriter.newLine();
            bufferWriter.close();
            writer.close();
        } catch (IOException e) {
            System.err.println("Error when write in file: " + cvs.getName() + e);
        }
    }

    private void toCSVFile(String str) {
        toCSVFile(str, CSVFILENAME);
    }

    private static void clearCSVFile() {
        File cvs = new File(System.getProperty("user.dir"));
        if (cvs.isDirectory()) {
            File[] reports = cvs.listFiles((dir, name) -> name.toLowerCase().endsWith(POSTFIX));
            if (reports != null) {
                for (File file : reports) {
                    try (FileWriter writer = new FileWriter(file)) {
                        BufferedWriter bufferedWriter = new BufferedWriter(writer);
                        bufferedWriter.write("");
                        bufferedWriter.close();
                    } catch (IOException e) {
                        System.err.println("Error when clear file: " + cvs.getName() + e);
                    }
                }
            }
        }
    }
}
