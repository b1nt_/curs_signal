package org.dadle8.points;

import org.dadle8.curs.ApConvolutionImpl;
import org.junit.Assert;
import org.junit.Test;

import java.util.logging.Level;

public class TestExecution {

    @Test
    public void test_aperiodic_convolution() {
        ApConvolutionImpl apConvolution = new ApConvolutionImpl(Level.OFF);
        double[] a = {2.0, 1.0, 3.0, -1.0};
        double[] b = {-1.0, 1.0, 2.0};

        double[] actual = apConvolution.execute(a, b);
        double[] result = apConvolution.executeWithFixedPoint(a, b);

        double[] expect = {-2, 1, 2, 6, 5, -2};
        double delta = 0.001;

        Assert.assertArrayEquals(expect, actual, delta);
        Assert.assertArrayEquals(expect, result, delta);
    }


    @Test
    public void test5() {
        FixedPoint[] array = {new FixedPoint(4,11, "-0.375"), new FixedPoint(4,11, "-2.0625"), new FixedPoint(4,11, "-12.2222")};

        for(FixedPoint fixedPoint : array) {
            System.out.println(fixedPoint.getBinaryStringValue() + "\t" + fixedPoint.getFixedPointValueWithTruncated(8) + "\n");
        }
    }
}
